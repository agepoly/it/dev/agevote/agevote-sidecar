import axios from "axios";
import { logger } from './utils/logger.js';

export function eventsPropagation(events: any) {
	events.forEach((record: any) => {
		const { event } = record;

		if (event.section !== "agevote") {
			logger.info(`Received event ${event.method} from blockchain, section ${event.section} : ignored`)
			return
		}

		let url = new URL(process.env.BACKEND_URL as string)
		let data = {}
		switch (event.method) {
			case "VoteAdded":
				url = new URL("/api/events/vote-added", url)
				data = {
					voteIndex: event.data[0],
					voteJson: event.data[1],
					status: event.data[2],
					who: event.data[3]
				}
				break
			case "VoteStatusUpdated":
				url = new URL("/api/events/vote-status-updated", url)
				data = {
					voteIndex: event.data[0],
					oldStatus: event.data[1],
					newStatus: event.data[2],
					who: event.data[3]
				}
				break
			case "VoteBallotAdded":
				url = new URL("/api/events/vote-ballot-added", url)
				data = {
					voteIndex: event.data[0],
					encryptedTellingToken: event.data[1],
					who: event.data[2]
				}
				break
			case "VoteResultsAdded":
				url = new URL("/api/events/vote-results-added", url)
				data = {
					voteIndex: event.data[0],
					encryptedBallots: event.data[1],
					results: event.data[2],
					who: event.data[3]
				}
				break
			case "VoteValidation":
				url = new URL("/api/events/vote-validation", url)
				data = {
					voteIndex: event.data[0],
					validations: event.data[1],
					who: event.data[2]
				}
				break
		}
		logger.info(`Received event ${event.method} from blockchain (${JSON.stringify(data)})`)
		axios.post(url.toString(), data, {headers: { Authorization: `Bearer ${process.env.SIDECAR_SECRET}` }    })
			.catch((error) => {
				logger.error('Axios error during event dispatch: ' + error)
				console.error(error)
			})
	});
}