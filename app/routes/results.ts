import { Router } from 'express';
import { api, key } from '../../app.js';
import { extractDispatchErrorMessage } from '../utils/polkadot.js';
import { logger } from '../utils/logger.js';

export const resultsRouter = Router();

resultsRouter.post('/add', async (req, res) => {
	const { voteIndex, results } = req.body;
	const resultsArray = [...Buffer.from(JSON.stringify(results.ballots))]
	const nonce = await api.rpc.system.accountNextIndex(key.publicKey);

	await api.tx.agevote.addResults(voteIndex, resultsArray).signAndSend(key, { nonce }, (result) => {
		if (result.status.isInBlock) {
			if (!result.dispatchError) {
				logger.info(`addResults in block ${result.status.asInBlock}`);
			} else {
				const errorMessage = extractDispatchErrorMessage(result.dispatchError, api)
				logger.error('Dispatch error for addResults: ' + errorMessage);
			}
		}
	})

	res.sendStatus(200)
});

resultsRouter.post('/validate', async (req, res) => {
	const { voteIndex } = req.body;
	const nonce = await api.rpc.system.accountNextIndex(key.publicKey);

	await api.tx.agevote.validateResults(voteIndex).signAndSend(key, { nonce }, (result) => {
		if (result.status.isInBlock) {
			logger.info(`validateResults in block ${result.status.asInBlock}`);
			if (result.dispatchError) {
				const errorMessage = extractDispatchErrorMessage(result.dispatchError, api)
				logger.error('Dispatch error for validateResults: ' + errorMessage);
			}
		}
	})

	res.sendStatus(200)
});