import { Router } from 'express';
import { api, key } from '../../app.js';
import { extractDispatchErrorMessage } from '../utils/polkadot.js';
import { logger } from '../utils/logger.js';

export const voteRouter = Router();

voteRouter.get('/:id', async (req, res) => {
	const voteIndex = req.params.id;

	var vote = await api.query.agevote.votesMap(voteIndex)

	if (vote.isEmpty) {
		res.sendStatus(404)
	} else {
		res.status(200).send(vote)
	}
})

voteRouter.post('/', async (req, res) => {
	const { voteIndex, status, voteJson } = req.body;
	const jsonArray = [...Buffer.from(JSON.stringify(voteJson))]
	const nonce = await api.rpc.system.accountNextIndex(key.publicKey);

	logger.info(`Adding a new vote: #${voteIndex}, voteJson: ${JSON.stringify(voteJson)}`)
	await api.tx.agevote.addVote(voteIndex, status, jsonArray).signAndSend(key, { nonce }, (result) => {
		if (result.status.isInBlock) {
			if (!result.dispatchError) {
				logger.info(`addVote included at blockHash ${result.status.asInBlock}`);
			} else {
				const errorMessage = extractDispatchErrorMessage(result.dispatchError, api)
				logger.error('Dispatch error (addVote): ' + errorMessage);
			}
		}
	})

	res.sendStatus(200)
});

voteRouter.get('/', async (req, res) => {
	const allEntries = await api.query.agevote.votesMap.entries()
    const votesMap = {} as any
    allEntries.forEach(([storageKey, codec]) => {
        const index = Buffer.from(storageKey.toHex().slice(-8), 'hex').readInt32LE()
        votesMap[index] = codec.toJSON()
    })
	logger.info(`Votes indexes: ${Object.keys(votesMap) || 'No votes'}`)
	res.send(votesMap)
});

voteRouter.patch('/', async (req, res) => {
	const { voteIndex, status } = req.body;
	const nonce = await api.rpc.system.accountNextIndex(key.publicKey);

	await api.tx.agevote.updateStatus(voteIndex, status).signAndSend(key, { nonce }, (result) => {
		if (result.status.isInBlock) {
			if (!result.dispatchError) {
				logger.info(`updateStatus in block ${result.status.asInBlock}`);
			} else {
				const errorMessage = extractDispatchErrorMessage(result.dispatchError, api)
				logger.error('Dispatch error for updateStatus: ' + errorMessage);
			}
		}
	})

	res.sendStatus(200)
});