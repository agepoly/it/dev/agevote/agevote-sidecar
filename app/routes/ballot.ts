import { Router } from 'express';
import { api, key } from '../../app.js';
import { extractDispatchErrorMessage } from '../utils/polkadot.js';
import { logger } from '../utils/logger.js';

export const ballotRouter = Router();

ballotRouter.post('/', async (req, res) => {
	const { voteIndex, encryptedBallot, encryptedTellingToken } = req.body;
	const nonce = await api.rpc.system.accountNextIndex(key.publicKey);

	await api.tx.agevote.addEncryptedBallot(voteIndex, encryptedBallot, encryptedTellingToken).signAndSend(key, { nonce }, (result) => {
		if (result.status.isInBlock) {
			if (!result.dispatchError) {
				logger.info(`addEncryptedBallot in block ${result.status.asInBlock}`);
			} else {
				const errorMessage = extractDispatchErrorMessage(result.dispatchError, api)
				logger.error('Dispatch error for addEncryptedBallot: ' + errorMessage);
			}
		}
	})

	res.sendStatus(200)
});