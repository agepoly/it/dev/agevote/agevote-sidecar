export function extractDispatchErrorMessage(dispatchError: any, api: any): string {
	let errorMessage = ''
	if (dispatchError.isModule) {
		const decoded = api.registry.findMetaError(dispatchError.asModule);
		const { docs, name, section } = decoded;
		errorMessage = `${section}.${name}: ${docs.join(' ')}`
	} else {
		errorMessage = dispatchError.toString()
	}
	return errorMessage
}
