import { createLogger, format, transports } from 'winston';

export const logger = createLogger({
	format: format.combine(
	  	format.timestamp(),
	  	format.printf(info => {
        	return `[${info.timestamp} - ${info.level.toUpperCase()}] ${info.message} ${info.meta != null ? JSON.stringify(info.meta) : ''}`
		}),
		format.colorize(),
	),
	transports: [new transports.Console()]
})