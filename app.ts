import express, { Express } from 'express';
import dotenv from 'dotenv';
import { WsProvider, ApiPromise, Keyring } from '@polkadot/api';
import { cryptoWaitReady } from '@polkadot/util-crypto';
import { voteRouter } from './app/routes/vote.js';
import { ballotRouter } from './app/routes/ballot.js';
import { resultsRouter } from './app/routes/results.js';
import { eventsPropagation } from './app/events.js';
import { logger } from './app/utils/logger.js';

dotenv.config();
const { BLOCKCHAIN_WS_URL, KEY_PHRASE, EXPRESS_ADDRESS, EXPRESS_PORT, SIDECAR_SECRET } = process.env

if (SIDECAR_SECRET == null || SIDECAR_SECRET.length === 0) {
	logger.error("Missing SIDECAR_SECRET. Abort.", {env: process.env})
	process.exit(1)
}

if (KEY_PHRASE == null || KEY_PHRASE.length === 0) {
	logger.error("Missing KEY_PHRASE. Abort.", {env: process.env})
	process.exit(1)
}

//Generate polkadot api
const wsProvider = new WsProvider(BLOCKCHAIN_WS_URL)
export const api = await ApiPromise.create({ provider: wsProvider })

//Generate API KEY
await cryptoWaitReady();
const keyring = new Keyring({ type: 'sr25519' });
export const key = keyring.addFromUri(KEY_PHRASE as string);

const app: Express = express();

app.use((req, res, next) => {
	logger.log({
		level: "info",
		message: `HTTP ${req.method.toUpperCase()} ${req.path}`,
		metadata: req
	});
	next()
})

app.use((req,res,next) => {
	if (SIDECAR_SECRET === 'insecure') return next()
	const token = req.headers.authorization?.split('Bearer ')[1];
	if (!token || token !== SIDECAR_SECRET) {
		logger.warn(`Forbidden call to ${req.path}`, req)
		res.sendStatus(403)
		return
	}
	next()
})

app.use(express.json())
app.use('/votes', voteRouter);
app.use('/ballots', ballotRouter);
app.use('/results', resultsRouter);

app.all('**', (req, res) => {
	res.status(404)
	res.send({
		error: 'not found'
	})
})

app.listen(parseInt((EXPRESS_PORT as string) || '9000'), (EXPRESS_ADDRESS as string) || '0.0.0.0', () => {
	logger.info(`⚡️ Server is running at http://${EXPRESS_ADDRESS}:${EXPRESS_PORT}`);
});

//Subscribe to system events via storage
api.query.system.events((events: any) => eventsPropagation(events));
